"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.rollupTypes = exports.isTypesRollupEnabled = void 0;
const tslib_1 = require("tslib");
const path_1 = tslib_1.__importDefault(require("path"));
const rollup_plugin_dts_1 = tslib_1.__importDefault(require("rollup-plugin-dts"));
const rollup_plugin_delete_1 = tslib_1.__importDefault(require("rollup-plugin-delete"));
const rollup_1 = require("rollup");
const tsconfig_1 = require("./tsconfig");
const constants_1 = require("./constants");
const utils_1 = require("./utils");
function descendantOfDist(declarationDir) {
    const relative = path_1.default.relative(constants_1.paths.appDist, (0, utils_1.resolveApp)(declarationDir));
    return (Boolean(relative) &&
        !relative.startsWith('..') &&
        !path_1.default.isAbsolute(relative));
}
function getTypesEntryPoint(appPackageJson) {
    // https://www.typescriptlang.org/docs/handbook/declaration-files/publishing.html#including-declarations-in-your-npm-package
    return appPackageJson.types || appPackageJson.typings;
}
function isTypesRollupEnabled(appPackageJson) {
    return Boolean(getTypesEntryPoint(appPackageJson));
}
exports.isTypesRollupEnabled = isTypesRollupEnabled;
async function rollupTypes(tsconfig, appPackageJson) {
    const tsCompilerOptions = (0, tsconfig_1.typescriptCompilerOptions)(tsconfig);
    const declarationDir = tsCompilerOptions.declarationDir || path_1.default.join('dist', 'types');
    // define bailout conditions
    // - when no 'typings' or 'types' entrypoint is defined in package.json
    // - when tsconfig.json `declaration` is explicitly set to false
    // - when `declarationDir` is not a descendant of `dist` (this must be a configuration error, but bailing out just to be safe)
    if (!isTypesRollupEnabled(appPackageJson) ||
        tsCompilerOptions.declaration === false ||
        !descendantOfDist(declarationDir)) {
        return;
    }
    const config = {
        input: path_1.default.join(declarationDir, 'index.d.ts'),
        output: { file: getTypesEntryPoint(appPackageJson), format: 'es' },
        plugins: [(0, rollup_plugin_dts_1.default)(), (0, rollup_plugin_delete_1.default)({ hook: 'buildEnd', targets: declarationDir })],
    };
    try {
        const bundle = await (0, rollup_1.rollup)(config);
        await bundle.write(config.output);
    }
    catch (e) {
        console.log('Failed to rollup types:', e);
    }
}
exports.rollupTypes = rollupTypes;
