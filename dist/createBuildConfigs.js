"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createBuildConfigs = void 0;
const tslib_1 = require("tslib");
const fs = tslib_1.__importStar(require("fs-extra"));
const chalk_1 = tslib_1.__importDefault(require("chalk"));
const constants_1 = require("./constants");
const createRollupConfig_1 = require("./createRollupConfig");
const logError_1 = tslib_1.__importDefault(require("./logError"));
const utils_1 = require("./utils");
const defaults_1 = require("./defaults");
async function createBuildConfigs(opts, appPackageJson) {
    const allInputs = createAllFormats(opts).map((options, index) => (Object.assign(Object.assign({}, options), { 
        // We want to know if this is the first run for each entryfile
        // for certain plugins (e.g. css)
        writeMeta: index === 0 })));
    // check for custom dts.config.ts/dts.config.js
    const dtsBuildConfig = getNormalizedDtsConfig();
    return await Promise.all(allInputs.map(async (options, index) => {
        // pass the full rollup config to dts-cli.config.js override
        const config = await (0, createRollupConfig_1.createRollupConfig)(appPackageJson, options, index);
        return dtsBuildConfig.rollup(config, options);
    }));
}
exports.createBuildConfigs = createBuildConfigs;
function createAllFormats(opts) {
    const sharedOpts = Object.assign(Object.assign({}, opts), { 
        // for multi-entry, we use an input object to specify where to put each
        // file instead of output.file
        input: opts.input.reduce((dict, input, index) => {
            dict[`${opts.output.file[index]}`] = input;
            return dict;
        }, {}), 
        // multiple UMD names aren't currently supported for multi-entry
        // (can't code-split UMD anyway)
        name: Array.isArray(opts.name) ? opts.name[0] : opts.name });
    return [
        opts.format.includes('cjs') && Object.assign(Object.assign({}, sharedOpts), { format: 'cjs', env: 'development' }),
        opts.format.includes('cjs') && Object.assign(Object.assign({}, sharedOpts), { format: 'cjs', env: 'production' }),
        opts.format.includes('esm') && Object.assign(Object.assign({}, sharedOpts), { format: 'esm' }),
        opts.format.includes('umd') && Object.assign(Object.assign({}, sharedOpts), { format: 'umd', env: 'development' }),
        opts.format.includes('umd') && Object.assign(Object.assign({}, sharedOpts), { format: 'umd', env: 'production' }),
        opts.format.includes('system') && Object.assign(Object.assign({}, sharedOpts), { format: 'system', env: 'development' }),
        opts.format.includes('system') && Object.assign(Object.assign({}, sharedOpts), { format: 'system', env: 'production' }),
    ].filter(Boolean);
}
function getNormalizedDtsConfig() {
    var _a;
    const dtsConfig = getDtsConfig();
    if (!dtsConfig.rollup) {
        console.log(chalk_1.default.yellow('rollup configuration not provided. Using default no-op configuration.'));
    }
    return Object.assign(Object.assign({}, dtsConfig), { rollup: (_a = dtsConfig.rollup) !== null && _a !== void 0 ? _a : defaults_1.configDefaults.rollup });
}
function getDtsConfig() {
    // check for custom dts.config.js
    let dtsConfig = defaults_1.configDefaults;
    if (fs.existsSync(constants_1.paths.appConfigTs)) {
        dtsConfig = loadDtsConfigTs();
    }
    else if (fs.existsSync(constants_1.paths.appConfigJs)) {
        dtsConfig = loadDtsConfigJs();
    }
    else if (fs.existsSync(constants_1.paths.appConfigCjs)) {
        dtsConfig = loadDtsConfigCjs();
    }
    return isDtsConfig(dtsConfig) ? dtsConfig : defaults_1.configDefaults;
}
// This can return undefined if they don't export anything in
// dts.config.ts
function loadDtsConfigTs() {
    try {
        require('ts-node').register({
            compilerOptions: {
                module: 'CommonJS',
            },
        });
        return (0, utils_1.interopRequireDefault)(require(constants_1.paths.appConfigTs)).default;
    }
    catch (error) {
        (0, logError_1.default)(error);
        process.exit(1);
    }
}
// This can return undefined if they don't export anything in
// dts.config.js
function loadDtsConfigJs() {
    // babel-node could easily be injected here if so desired.
    return require(constants_1.paths.appConfigJs);
}
function loadDtsConfigCjs() {
    return require(constants_1.paths.appConfigCjs);
}
function isDtsConfig(required) {
    return isDefined(required) && isDefined(required);
}
function isDefined(required) {
    return required !== null && required !== undefined;
}
