"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.typescriptCompilerOptions = void 0;
const tslib_1 = require("tslib");
const typescript_1 = tslib_1.__importDefault(require("typescript"));
const constants_1 = require("./constants");
function typescriptCompilerOptions(tsconfig) {
    const tsconfigPath = tsconfig || constants_1.paths.tsconfigJson;
    // borrowed from https://github.com/facebook/create-react-app/pull/7248
    const tsconfigJSON = typescript_1.default.readConfigFile(tsconfigPath, typescript_1.default.sys.readFile).config;
    // borrowed from https://github.com/ezolenko/rollup-plugin-typescript2/blob/42173460541b0c444326bf14f2c8c27269c4cb11/src/parse-tsconfig.ts#L48
    return typescript_1.default.parseJsonConfigFileContent(tsconfigJSON, typescript_1.default.sys, './').options;
}
exports.typescriptCompilerOptions = typescriptCompilerOptions;
