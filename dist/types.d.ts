import { RollupOptions } from 'rollup';
interface SharedOpts {
    target: 'node' | 'browser';
    tsconfig?: string;
    extractErrors?: boolean;
}
export declare type ModuleFormat = 'cjs' | 'umd' | 'esm' | 'system';
export interface BuildOpts extends SharedOpts {
    name?: string;
    entry?: string | string[];
    format: 'cjs,esm';
    target: 'browser';
    noClean?: boolean;
    rollupTypes?: boolean;
}
export interface WatchOpts extends BuildOpts {
    verbose?: boolean;
    onFirstSuccess?: string;
    onSuccess?: string;
    onFailure?: string;
}
export interface NormalizedOpts extends Omit<WatchOpts, 'name' | 'input' | 'format'> {
    name: string | string[];
    input: string[];
    format: [ModuleFormat, ...ModuleFormat[]];
    output: {
        file: string[];
    };
}
export declare type DtsOptionsInput = {
    [entryAlias: string]: string;
};
export interface DtsOptions extends SharedOpts {
    name: string;
    input: string | DtsOptionsInput;
    env: 'development' | 'production';
    format: ModuleFormat;
    minify?: boolean;
    writeMeta?: boolean;
    transpileOnly?: boolean;
    rollupTypes?: boolean;
}
export interface PackageJson {
    name: string;
    source?: string;
    jest?: any;
    eslint?: any;
    dependencies?: {
        [packageName: string]: string | undefined;
    };
    devDependencies?: {
        [packageName: string]: string | undefined;
    };
    engines?: {
        node?: string;
    };
    types?: string;
    typings?: string;
}
export interface DtsConfig {
    rollup?: (config: RollupOptions, options: DtsOptions) => RollupOptions;
}
export interface NormalizedDtsConfig extends DtsConfig {
    rollup: (config: RollupOptions, options: DtsOptions) => RollupOptions;
}
export {};
