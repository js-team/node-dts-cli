"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.paths = void 0;
const utils_1 = require("./utils");
exports.paths = {
    appPackageJson: (0, utils_1.resolveApp)('package.json'),
    tsconfigJson: (0, utils_1.resolveApp)('tsconfig.json'),
    testsSetup: (0, utils_1.resolveApp)('test/setupTests.ts'),
    appRoot: (0, utils_1.resolveApp)('.'),
    appSrc: (0, utils_1.resolveApp)('src'),
    appErrorsJson: (0, utils_1.resolveApp)('errors/codes.json'),
    appErrors: (0, utils_1.resolveApp)('errors'),
    appDist: (0, utils_1.resolveApp)('dist'),
    appConfigJs: (0, utils_1.resolveApp)('dts.config.js'),
    appConfigTs: (0, utils_1.resolveApp)('dts.config.ts'),
    appConfigCjs: (0, utils_1.resolveApp)('dts.config.cjs'),
    jestConfig: (0, utils_1.resolveApp)('jest.config.js'),
    progressEstimatorCache: (0, utils_1.resolveApp)('node_modules/.cache/.progress-estimator'),
};
