#!/usr/bin/env node
import { RollupOptions } from 'rollup';
import { DtsConfig, DtsOptions } from './types';
export declare const isDir: (name: string) => Promise<boolean>;
export declare const isFile: (name: string) => Promise<boolean>;
export declare const defineConfig: (config: DtsConfig) => DtsConfig;
export { DtsConfig, DtsOptions, RollupOptions };
