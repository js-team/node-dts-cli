export declare const paths: {
    appPackageJson: string;
    tsconfigJson: string;
    testsSetup: string;
    appRoot: string;
    appSrc: string;
    appErrorsJson: string;
    appErrors: string;
    appDist: string;
    appConfigJs: string;
    appConfigTs: string;
    appConfigCjs: string;
    jestConfig: string;
    progressEstimatorCache: string;
};
