"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createJestConfig = void 0;
function createJestConfig(_, rootDir) {
    const config = {
        transform: {
            '.(ts|tsx)$': 'ts-jest/dist',
            '.(js|jsx)$': 'babel-jest', // jest's default
        },
        transformIgnorePatterns: ['[/\\\\]node_modules[/\\\\].+\\.(js|jsx)$'],
        moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
        collectCoverageFrom: ['src/**/*.{ts,tsx,js,jsx}'],
        testMatch: ['<rootDir>/**/*.(spec|test).{ts,tsx,js,jsx}'],
        testURL: 'http://localhost',
        rootDir,
        watchPlugins: [
            'jest-watch-typeahead/filename',
            'jest-watch-typeahead/testname',
        ],
    };
    return config;
}
exports.createJestConfig = createJestConfig;
