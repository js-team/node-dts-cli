import { Template } from '../template';
interface ProjectArgs {
    name: string;
    author: string;
    includeHuskyConfig: boolean;
}
export declare const composePackageJson: (template: Template) => ({ name, author, includeHuskyConfig }: ProjectArgs) => {
    name: string;
    author: string;
    module: string;
    'size-limit': {
        path: string;
        limit: string;
    }[];
    version?: string | undefined;
    description?: string | undefined;
    keywords?: string[] | undefined;
    homepage?: import("type-fest").LiteralUnion<".", string> | undefined;
    bugs?: import("type-fest").PackageJson.BugsLocation | undefined;
    license?: string | undefined;
    licenses?: {
        type?: string | undefined;
        url?: string | undefined;
    }[] | undefined;
    contributors?: import("type-fest").PackageJson.Person[] | undefined;
    maintainers?: import("type-fest").PackageJson.Person[] | undefined;
    files?: string[] | undefined;
    type?: "module" | "commonjs" | undefined;
    main?: string | undefined;
    exports?: import("type-fest").PackageJson.Exports | undefined;
    imports?: import("type-fest").PackageJson.Imports | undefined;
    bin?: string | Partial<Record<string, string>> | undefined;
    man?: string | string[] | undefined;
    directories?: import("type-fest").PackageJson.DirectoryLocations | undefined;
    repository?: string | {
        type: string;
        url: string;
        directory?: string | undefined;
    } | undefined;
    scripts?: import("type-fest").PackageJson.Scripts | undefined;
    config?: Record<string, unknown> | undefined;
    dependencies?: Partial<Record<string, string>> | undefined;
    devDependencies?: Partial<Record<string, string>> | undefined;
    optionalDependencies?: Partial<Record<string, string>> | undefined;
    peerDependencies?: Partial<Record<string, string>> | undefined;
    peerDependenciesMeta?: Partial<Record<string, {
        optional: true;
    }>> | undefined;
    bundledDependencies?: string[] | undefined;
    bundleDependencies?: string[] | undefined;
    engines?: {
        [x: string]: string | undefined;
    } | undefined;
    engineStrict?: boolean | undefined;
    os?: import("type-fest").LiteralUnion<"aix" | "darwin" | "freebsd" | "linux" | "openbsd" | "sunos" | "win32" | "!aix" | "!darwin" | "!freebsd" | "!linux" | "!openbsd" | "!sunos" | "!win32", string>[] | undefined;
    cpu?: import("type-fest").LiteralUnion<"arm" | "arm64" | "ia32" | "mips" | "mipsel" | "ppc" | "ppc64" | "s390" | "s390x" | "x32" | "x64" | "!arm" | "!arm64" | "!ia32" | "!mips" | "!mipsel" | "!ppc" | "!ppc64" | "!s390" | "!s390x" | "!x32" | "!x64", string>[] | undefined;
    preferGlobal?: boolean | undefined;
    private?: boolean | undefined;
    publishConfig?: import("type-fest").PackageJson.PublishConfig | undefined;
    funding?: string | {
        type?: import("type-fest").LiteralUnion<"github" | "opencollective" | "patreon" | "individual" | "foundation" | "corporation", string> | undefined;
        url: string;
    } | undefined;
    esnext?: string | {
        [moduleName: string]: string | undefined;
        main?: string | undefined;
        browser?: string | undefined;
    } | undefined;
    browser?: string | Partial<Record<string, string | false>> | undefined;
    sideEffects?: boolean | string[] | undefined;
    types?: string | undefined;
    typesVersions?: Partial<Record<string, Partial<Record<string, string[]>>>> | undefined;
    typings?: string | undefined;
    workspaces?: string[] | import("type-fest").PackageJson.WorkspaceConfig | undefined;
    flat?: boolean | undefined;
    resolutions?: Partial<Record<string, string>> | undefined;
    jspm?: import("type-fest").PackageJson | undefined;
    husky?: any;
    prettier?: any;
    jest?: any;
};
interface DependencyArgs {
    includeHusky: boolean;
}
export declare const composeDependencies: (template: Template) => ({ includeHusky }: DependencyArgs) => string[];
export {};
