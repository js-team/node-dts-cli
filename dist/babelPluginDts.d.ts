export declare const isTruthy: (obj?: any) => boolean;
export declare const mergeConfigItems: (type: any, ...configItemsToMerge: any[]) => any[];
export declare const createConfigItems: (type: any, items: any[]) => any[][];
export declare const babelPluginDts: typeof import("@rollup/plugin-babel").getBabelInputPlugin;
