import { CompilerOptions } from 'typescript';
export declare function typescriptCompilerOptions(tsconfig: string | undefined): CompilerOptions;
